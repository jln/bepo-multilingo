# bepo-multilingo

Disposition bépo pour le clavier android [Multiling O Keyboard](https://play.google.com/store/apps/details?id=kl.ime.oh&hl=en_US).

## Description

Ses principales caractéristiques sont les suivantes :

- À l'exception du ''é'', pas d'affichage des lettres accentuées, afin de réduire la nombre de touches à afficher et donc conserver une largeur de touche acceptable.
- L'appui long sans déplacement insère *toujours* le symbole affiché sur le clavier (lorsque l'option d'affichage des symboles est activée).
- Autant que faire se peut, respect de la position des caractères de ponctuation

## Historique

- 2019-08-17 : utilisation du point pour la multiplication (U+22C5) à la place du point médian, ce dernier se comportant comme une touche morte
- 2019-08-17 : ouverture du dépôt et publication de la version 190817a
